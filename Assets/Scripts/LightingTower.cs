using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class LightingTower : MonoBehaviour
{
    private ARRaycastManager raycastManager;
    private ARPlaneManager planeManager;
    private List<ARRaycastHit> hits = new List<ARRaycastHit>();

    [SerializeField] GameObject towerPrefab;
    [SerializeField] ParticleSystem firePrefab;
    [SerializeField] ParticleSystem soulPrefab;
    [SerializeField] ParticleSystem armagedonPrefab;
    [SerializeField] GameObject portalPrefab;

    private Camera arCam;

    private bool towerPlaced;

    private string activeButtonTag;
    private GameObject tower;

    [SerializeField] UnityEngine.UI.Text Log;

    private Vector3 effectCoordinates = new Vector3();

    private ParticleSystem currentEffect;
    private GameObject currentEffectObject;

    // Start is called before the first frame update
    void Start()
    {
        raycastManager = GetComponent<ARRaycastManager>();
        planeManager = GetComponent<ARPlaneManager>();

        arCam = GameObject.Find("AR Camera").GetComponent<Camera>();

        towerPlaced = false;

        activeButtonTag = "empty";
        tower = null;

        Log.text = "initialized";

        currentEffect = null;
        currentEffectObject = null;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount == 0 || Input.GetTouch(0).phase != TouchPhase.Began)
        {
            return;
        }

        if (!towerPlaced)
        {
            if (raycastManager.Raycast(Input.GetTouch(0).position, hits, TrackableType.Planes))
            {
                tower = Instantiate(towerPrefab, hits[0].pose.position, Quaternion.identity);
                towerPlaced = true;

                Log.text = "tower placed";

                effectCoordinates = new Vector3((float)(tower.transform.position.x + 0.5), (float)(tower.transform.position.y + 2.5), (float)(tower.transform.position.z - 0.5));

                planeManager.enabled = false;
                foreach (var Plane in planeManager.trackables)
                {
                    Plane.gameObject.SetActive(false);
                }
            }
            return;
        }

        RaycastHit hit;
        Ray ray = arCam.ScreenPointToRay(Input.GetTouch(0).position);

        if (towerPlaced && raycastManager.Raycast(Input.GetTouch(0).position, hits))
        {
            if (Physics.Raycast(ray, out hit))
            {
                Log.text = hit.collider.gameObject.tag;

                if (hit.collider.gameObject.tag == "fireButton")
                {
                    if (activeButtonTag == "empty")
                    {
                        currentEffect = Instantiate(firePrefab, effectCoordinates, Quaternion.identity);
                        currentEffect.transform.Rotate(-90, 0, 0);
                        activeButtonTag = "fireButton";
                    }
                    else if (activeButtonTag == "fireButton")
                    {
                        Destroy(currentEffect);
                        activeButtonTag = "empty";
                    }
                    else
                    {
                        Destroy(currentEffect);
                        Destroy(currentEffectObject);
                        currentEffect = Instantiate(firePrefab, effectCoordinates, Quaternion.identity);
                        currentEffect.transform.Rotate(-90, 0, 0);
                        activeButtonTag = "fireButton";
                    }
                    return;
                }
            
                else if (hit.collider.gameObject.tag == "armagedonButton")
                {
                    if (activeButtonTag == "empty")
                    {
                        currentEffect = Instantiate(armagedonPrefab, effectCoordinates, Quaternion.identity);
                        currentEffect.transform.Rotate(-90, 0, 0);
                        activeButtonTag = "armagedonButton";
                    }
                    else if (activeButtonTag == "armagedonButton")
                    {
                        Destroy(currentEffect);
                        activeButtonTag = "empty";
                    }
                    else
                    {
                        Destroy(currentEffect);
                        Destroy(currentEffectObject);
                        currentEffect = Instantiate(armagedonPrefab, effectCoordinates, Quaternion.identity);
                        currentEffect.transform.Rotate(-90, 0, 0);
                        activeButtonTag = "armagedonButton";
                    }
                    return;
                }

                else if (hit.collider.gameObject.tag == "soulButton")
                {
                    if (activeButtonTag == "empty")
                    {
                        currentEffect = Instantiate(soulPrefab, effectCoordinates, Quaternion.identity);
                        currentEffect.transform.Rotate(-90, 0, 0);
                        activeButtonTag = "soulButton";
                    }
                    else if (activeButtonTag == "soulButton")
                    {
                        Destroy(currentEffect);
                        activeButtonTag = "empty";
                    }
                    else
                    {
                        Destroy(currentEffect);
                        Destroy(currentEffectObject);
                        currentEffect = Instantiate(soulPrefab, effectCoordinates, Quaternion.identity);
                        currentEffect.transform.Rotate(-90, 0, 0);
                        activeButtonTag = "soulButton";
                    }
                    return;
                }

                else if (hit.collider.gameObject.tag == "portalButton")
                {
                    if (activeButtonTag == "empty")
                    {
                        currentEffectObject = Instantiate(portalPrefab, effectCoordinates, Quaternion.identity);
                        currentEffectObject.transform.Rotate(-90, 0, 0);
                        activeButtonTag = "portalButton";
                    }
                    else if (activeButtonTag == "portalButton")
                    {
                        Destroy(currentEffectObject);
                        activeButtonTag = "empty";
                    }
                    else
                    {
                        Destroy(currentEffect);
                        currentEffectObject = Instantiate(portalPrefab, effectCoordinates, Quaternion.identity);
                        currentEffectObject.transform.Rotate(-90, 0, 0);
                        activeButtonTag = "portalButton";
                    }
                    return;
                }
            }
            return;
        }
    }
}
